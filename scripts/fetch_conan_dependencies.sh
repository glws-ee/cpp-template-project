#!/usr/bin/env bash

# Cpp Template Project: A template CMake project to get you started with
# C++ and tooling.
#
# Copyright 2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

function fetch
{
    local -r cc="${1}"
    local -r cxx="${2}"
    local -r type="${3}"

    echo -e "\033[0;33mFetching Conan dependencies for CC=${cc}, CXX=${cxx}, CMAKE_BUILD_TYPE=${type} configuration...\033[0m"

    # 1. We use `bash -c` here to catch our new `BASH_ENV`.
    # https://stackoverflow.com/a/29023834
    CC="${cc}" CXX="${cxx}" cmake -S. -Bbuild -DFETCH_CONAN_DEPENDENCIES=1 -DCMAKE_BUILD_TYPE="${type}"

    # 2. Clean all build data, otherwise conan will ignore new CC and CXX variables.
    rm -rf build

    # 3. Clean-up conan cache
    # https://stackoverflow.com/questions/50247671
    conan remove "*" -s -f
}

fetch 'clang' 'clang++' 'Debug'
fetch 'clang' 'clang++' 'Release'
fetch 'gcc' 'g++' 'Debug'
fetch 'gcc' 'g++' 'Release'
