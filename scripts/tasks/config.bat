:: Cpp Template Project: A template CMake project to get you started with
:: C++ and tooling.
::
:: Copyright 2020-2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
::
:: Licensed under the Apache License, Version 2.0 (the "License");
:: you may not use this file except in compliance with the License.
:: You may obtain a copy of the License at
::
::   http://www.apache.org/licenses/LICENSE-2.0
::
:: Unless required by applicable law or agreed to in writing, software
:: distributed under the License is distributed on an "AS IS" BASIS,
:: WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
:: See the License for the specific language governing permissions and
:: limitations under the License.

set BUILD_TYPE=%1
set BUILD_FOLDER=.\\build\\%1

if not exist %BUILD_FOLDER% mkdir %BUILD_FOLDER%

echo Run: cmake -S. -B %BUILD_FOLDER% -DCMAKE_BUILD_TYPE=%BUILD_TYPE%
cmake -S. -B %BUILD_FOLDER% -DCMAKE_BUILD_TYPE=%BUILD_TYPE%
