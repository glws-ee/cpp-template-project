#!/usr/bin/env bash
#
# FIX Testing Suite: Testing suite for FIX connections.
#
# Copyright 2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

declare BUILD_TYPE="${1}"
declare BUILD_FOLDER="./build/${BUILD_TYPE}"

if [[ ! -d "${BUILD_FOLDER}" ]]; then
  mkdir -p "${BUILD_FOLDER}"
fi

echo -e "\033[1;33mcmake -S. -B "${BUILD_FOLDER}" -DCMAKE_BUILD_TYPE=${BUILD_TYPE}\033[0m"
cmake -S. -B "${BUILD_FOLDER}" -DCMAKE_BUILD_TYPE=${BUILD_TYPE}
