#!/usr/bin/env bash

# Cpp Template Project: A template CMake project to get you started with
# C++ and tooling.
#
# Copyright 2020-2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

#------------------------------------------------------------------------------#
# Packages                                                                     #
#------------------------------------------------------------------------------#

declare -a INSTALL_PKGS=(
  "clang-12"
  "cmake"
  "cppcheck"
  "doxygen"
  "g++-10"
  "gcc-10"
  "git"
  "lcov"
  "make"
  "python3-pip"
)

# Install

# set noninteractive installation
export DEBIAN_FRONTEND=noninteractive
export TZ=Europe/Kiev

# In case we need a new version of clang, see https://apt.llvm.org/.
# deb http://apt.llvm.org/focal/ llvm-toolchain-focal-13 main
# deb-src http://apt.llvm.org/focal/ llvm-toolchain-focal-13 main

apt-get update
apt-get install --no-install-recommends -y ${INSTALL_PKGS[@]}

# Clean

apt-get clean
rm -rf /var/lib/apt/lists/*

#------------------------------------------------------------------------------#
# Packages Aliases                                                             #
#------------------------------------------------------------------------------#

# See https://stackoverflow.com/questions/7832892
# See https://stackoverflow.com/questions/67298443

# Clang

update-alternatives \
  --install /usr/bin/clang clang /usr/bin/clang-12 100 \
  --slave /usr/bin/clang++ clang++ /usr/bin/clang++-12 \

# GCC

update-alternatives --install /usr/bin/cpp cpp /usr/bin/cpp-10 110

update-alternatives \
  --install /usr/bin/gcc gcc /usr/bin/gcc-10 110 \
  --slave /usr/bin/g++ g++ /usr/bin/g++-10 \
  --slave /usr/bin/gcov gcov /usr/bin/gcov-10 \
  --slave /usr/bin/gcc-ar gcc-ar /usr/bin/gcc-ar-10 \
  --slave /usr/bin/gcc-ranlib gcc-ranlib /usr/bin/gcc-ranlib-10

#------------------------------------------------------------------------------#
# Python                                                                       #
#------------------------------------------------------------------------------#

pip3 install -r 'requirements.txt'
