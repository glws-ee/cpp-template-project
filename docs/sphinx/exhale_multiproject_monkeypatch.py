# -*- coding: utf-8 -*-

# Monkey patch exhale to support multiple doxygen projects at one time.
# Based on https://github.com/mithro/sphinx-contrib-mithro/tree/master/sphinx-contrib-exhale-multiproject
# Because of https://github.com/svenevs/exhale/issues/27

import exhale
import exhale.configs
import exhale.utils
import exhale.deploy

import os
import os.path
import types
from pprint import pprint

class MonkeyPatch:

    def __init__(self) -> None:
        self._current_project = None
        self._debug = os.environ.get('ENABLE_EXHALE_DEBUG_OUTPUT', False)

    # Patched methods

    def exhaleEnvironmentReady(self, app) -> None:
        self._current_project = app.config.breathe_default_project
        default_project = self._current_project

        default_exhale_args = dict(app.config.exhale_args)

        exhale_projects_args = dict(app.config._raw_config['exhale_projects_args'])
        breathe_projects = dict(app.config._raw_config['breathe_projects'])

        for project in breathe_projects:
            self._current_project = project
            app.config.breathe_default_project = project
            os.makedirs(breathe_projects[project], exist_ok=True)

            project_exhale_args = exhale_projects_args.get(project, {})

            app.config.exhale_args = dict(default_exhale_args)
            app.config.exhale_args.update(project_exhale_args)
            app.config.exhale_args["containmentFolder"] = os.path.realpath(app.config.exhale_args["containmentFolder"])

            if self._debug:
                print("="*75)
                print(project)
                print("-"*50)
                pprint(app.config.exhale_args)
                print("="*75)

            # First, setup the extension and verify all of the configurations.
            exhale.configs.apply_sphinx_configurations(app)
            ####### Next, perform any cleanup

            # Generate the full API!
            try:
                exhale.deploy.explode()
            except:
                exhale.utils.fancyError("Exhale: could not generate reStructuredText documents :/")

        app.config.breathe_default_project = default_project

    def makeCustomSpecificationsMapping(self, func):
        # Make sure they gave us a function
        if not isinstance(func, types.FunctionType):
            raise ValueError(
                f"The input to exhale.util.makeCustomSpecificationsMapping was *NOT* a function: {type(func)}"
            )

        return exhale.utils.makeCustomSpecificationsMapping(self._patchSpec(func))

    def specificationsForKind(self):
        return self._patchSpec(exhale.utils.specificationsForKind)

    # Utility methods

    def _patchSpec(self, func):
        def wrap(kind):
            spec = func(kind)
            spec.insert(0, f':project: {self._current_project}')
            return spec

        return wrap


def exhaleEnvironmentReady(app):
    mp = MonkeyPatch()

    exhale.utils.makeCustomSpecificationsMapping = \
    lambda func: mp.makeCustomSpecificationsMapping(func)

    exhale.utils.specificationsForKind = mp.specificationsForKind()

    mp.exhaleEnvironmentReady(app)


exhale.environment_ready = exhaleEnvironmentReady
