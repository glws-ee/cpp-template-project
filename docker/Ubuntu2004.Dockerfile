# Cpp Template Project: A template CMake project to get you started with
# C++ and tooling.
#
# Copyright 2020-2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM ubuntu:20.04

WORKDIR /tmp

# Setup system and install packages

COPY scripts/os/setup_ubuntu2004.sh .
COPY requirements.txt .
RUN bash setup_ubuntu2004.sh

# Fetch Conan dependencies

COPY cmake/Conan.cmake CMakeLists.txt
COPY scripts/fetch_conan_dependencies.sh .
RUN bash fetch_conan_dependencies.sh

# Remove all temporary files

WORKDIR /work
RUN rm -rf /tmp/
