/*
 * Cpp Template Project: A template CMake project to get you started with
 * C++ and tooling.
 *
 * Copyright 2020-2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTP_SHAPES_RECTANGLE_HPP
#define CTP_SHAPES_RECTANGLE_HPP

#include "common/point.hpp"

#include <boost/noncopyable.hpp>

namespace Ctp {
namespace Shapes {

/**
 * @brief Screen rectangle.
 *
 * The interface provides an abstraction for a screen rectangle. Such a rectangle
 * has sides that are parallel to the axis of the [Cartesian coordinate system][1].
 * This means that rectangle always comes up with four right angles.
 *
 * [1]: <https://en.wikipedia.org/wiki/Cartesian_coordinate_system>
 *
 * @warning @c X coodinate MUST increate its value from left to right.
 * @warning @c Y coodinate MUST increate its value from bottom to top.
 */
class Rectangle
    :   private boost::noncopyable
{

public:

    /**
     * @brief Default virtual desctuctor.
     */
    virtual ~Rectangle () = default;

    /**
     * @brief Get the top left point.
     *
     * @return The top left point.
     */
    [[nodiscard]]
    virtual Common::Point getTopLeft () const = 0;

    /**
     * @brief Get the top right point.
     *
     * @return The top right point.
     */
    [[nodiscard]]
    virtual Common::Point getTopRight () const = 0;

    /**
     * @brief Get the bottom left point.
     *
     * @return The bottom left point.
     */
    [[nodiscard]]
    virtual Common::Point getBottomLeft () const = 0;

    /**
     * @brief Get the bottom right point.
     *
     * @return The bottom right point.
     */
    [[nodiscard]]
    virtual Common::Point getBottomRight () const = 0;

    /**
     * @brief Get the width of the rectangle.
     *
     * @return The width of the rectangle.
     */
    [[nodiscard]]
    virtual double getWidth () const = 0;

    /**
     * @brief Get the height of the rectangle.
     *
     * @return The height of the rectangle.
     */
    [[nodiscard]]
    virtual double getHeight () const = 0;

    /**
     * @brief Get the perimeter of the rectangle.
     *
     * @return The perimeter of the rectangle.
     */
    [[nodiscard]]
    virtual double getPerimeter () const = 0;

    /**
     * @brief Get the area of the rectangle.
     *
     * @return The area of the rectangle.
     */
    [[nodiscard]]
    virtual double getArea () const = 0;

    /**
     * @brief Checks if the rectangle contains the given point.
     *
     * @return @c true if there is such a point, otherwise @c false.
     */
    [[nodiscard]]
    virtual bool contains ( Common::Point _point ) const = 0;

    /**
     * @brief Checks if two rectangles intersects.
     *
     * @return @c true if intersects, otherwise @c false.
     */
    [[nodiscard]]
    virtual bool intersects ( Rectangle const & _other ) const = 0;

    /**
     * @brief Checks if rectangle covers the given one.
     *
     * @return @c true if rectangle covers the given one, otherwise @c false.
     */
    [[nodiscard]]
    virtual bool covers ( Rectangle const & _other ) const = 0;

};

} // namespace Shapes
} // namespace Ctp

#endif // CTP_SHAPES_RECTANGLE_HPP
