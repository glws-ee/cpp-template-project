/*
 * Cpp Template Project: A template CMake project to get you started with
 * C++ and tooling.
 *
 * Copyright 2020-2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTP_SHAPES_SHAPESFACTORY_HPP
#define CTP_SHAPES_SHAPESFACTORY_HPP

#include "shapes/rectangle.hpp"

#include <memory>

namespace Ctp {
namespace Shapes {

/**
 * @brief The factory for creating @c Rectangle instance.
 */
class ShapesFactory
    :   private boost::noncopyable
{

public:

    /**
     * @brief Default virtual desctuctor.
     */
    virtual ~ShapesFactory () = default;

    /**
     * @brief Creates a @c Rectangle instance.
     *
     * @param _topLeftPoint The top left point of the rectangle.
     * @param _rightBottomPoint The right bottom point of the rectangle.
     *
     * @throw std::logic_error Thrown if @c _topLeftPoint has coordinates that
     * are below @c _rightBottomPoint or are pointing to the right of
     * @c _rightBottomPoint .
     *
     * @return @c Rectangle object if it was created, @c nullptr otherwise.
     */
    //@{
    [[nodiscard]]
    virtual std::unique_ptr< Rectangle > createRectangle (
        Common::Point _topLeftPoint,
        Common::Point _rightBottomPoint
    ) const = 0;

    /**
     * @param _point The starting point of the rectangle.
     * @param _width The width of the rectangle.
     * @param _height The height of the rectangle.
     *
     * @throw std::logic_error Thrown if @c _topLeftPoint has negative coordinates.
     *
     */
    [[nodiscard]]
    virtual std::unique_ptr< Rectangle > createRectangle (
        Common::Point _point,
        double _width,
        double _height
    ) const = 0;
    //@}

};

} // namespace Shapes
} // namespace Ctp

#endif // CTP_SHAPES_SHAPESFACTORY_HPP

