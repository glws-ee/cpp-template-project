/*
 * Cpp Template Project: A template CMake project to get you started with
 * C++ and tooling.
 *
 * Copyright 2020-2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTP_VCS_SRC_ACCESSORIMPL_HPP
#define CTP_VCS_SRC_ACCESSORIMPL_HPP

#include "vcs/accessor.hpp"

namespace Ctp {
namespace Vcs {

class AccessorImpl final
    :   public Accessor
{

public:

    std::unique_ptr< ReadOnlyVcsInfo > createVcsInfo () const override;

};

} // namespace Vcs
} // namespace Ctp

#endif // CTP_VCS_SRC_ACCESSORIMPL_HPP
