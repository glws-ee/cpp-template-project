/*
 * Cpp Template Project: A template CMake project to get you started with
 * C++ and tooling.
 *
 * Copyright 2020-2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTP_VCS_READONLYVCSINFO_HPP
#define CTP_VCS_READONLYVCSINFO_HPP

#include <boost/noncopyable.hpp>

#include <string_view>

namespace Ctp {
namespace Vcs {

/**
 * @brief Read-only interface for metadata of VCS.
 *
 * The interface provides an access to a different "version control system" information,
 * that has been available during the build process.
 */
class ReadOnlyVcsInfo
    :   private boost::noncopyable
{

public:

    /**
     * @brief Default virtual desctuctor.
     */
    virtual ~ReadOnlyVcsInfo () = default;

    /**
     * @brief Checks if the metadata was populated.
     *
     * @note There may be no metadata if there wasn't any VCS supplied directory
     * (e.g. downloaded source code without revision history).
     *
     * @return @c true if the metadata was populated, @c false otherwise.
     */
    [[nodiscard]]
    virtual bool isPopulated () const = 0;

    /**
     * @brief Checks if there any uncommitted changes that won't be reflected
     * in the CommitID.
     * @return @c true if there any uncommitted change, @c false otherwise.
     */
    [[nodiscard]]
    virtual bool hasUncommittedChanges () const = 0;

    /**
     * @brief Get an author's name of the last commit in the repository.
     * @return The author's name.
     */
    [[nodiscard]]
    virtual std::string_view getAuthorName () const = 0;

    /**
     * @brief Get an author's email of the last commit in the repository.
     * @return The author's email.
     */
    [[nodiscard]]
    virtual std::string_view getAuthorEmail () const = 0;

    /**
     * @brief Get a SHA-1 of the last commit in the repository.
     * @return The SHA-1 value.
     */
    [[nodiscard]]
    virtual std::string_view getCommitSha1 () const = 0;

    /**
     * @brief Get a date of the last commit in the repository in the ISO8601 format.
     * @return The ISO8601 date.
     */
    [[nodiscard]]
    virtual std::string_view getCommitDate () const = 0;

    /**
     * @brief Get a subject of the last commit in the repository.
     * @return The commit subject.
     */
    [[nodiscard]]
    virtual std::string_view getCommitSubject () const = 0;

    /**
     * @brief Get a body of the last commit in the repository.
     * @return The commit body.
     */
    [[nodiscard]]
    virtual std::string_view getCommitBody () const = 0;

    /**
     * @brief Get a description of the last commit in the repository.
     * @return The commit description.
     */
    [[nodiscard]]
    virtual std::string_view getDescribe () const = 0;

};

} // namespace Vcs
} // namespace Ctp

#endif // CTP_VCS_READONLYVCSINFO_HPP
