/*
 * Cpp Template Project: A template CMake project to get you started with
 * C++ and tooling.
 *
 * Copyright 2020 Zinchenko Serhii <zinchenko.serhii@pm.me>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "vcs/accessor.hpp"

#include <docopt/docopt.h>

#include <functional>
#include <iostream>

static constexpr auto s_usage =
R"(Cpp template project.
    Usage:
          example (-h | --help)
          example --version
 Options:
          -h --help     Show this screen.
          -v --version  Show version.
)";

int main ( int _argc, const char **_argv )
{
    auto args = docopt::docopt(
      s_usage,
      { std::next( _argv ), std::next( _argv, _argc ) },
      true,// show help if requested
      "Cpp template project 1.0"// version string
    );

    if ( args[ "--version" ].asBool() )
    {
        auto pAccessor = Ctp::Vcs::Accessor::create();
        auto pVcs = pAccessor->createVcsInfo();

        if ( pVcs->isPopulated() )
        {
            if ( pVcs->hasUncommittedChanges() )
            {
                std::cerr << "WARN: there were uncommitted changes at build-time." << std::endl;
            }

            std::cout << "Commit " << pVcs->getCommitSha1() << " (HEAD)\n"
                << "Describe " << pVcs->getDescribe() << "\n"
                << "Author: " << pVcs->getAuthorName() << " <" << pVcs->getAuthorEmail() << ">\n"
                << "Date: " << pVcs->getCommitDate() << "\n\n"
                << pVcs->getCommitSubject() << "\n"
                << pVcs->getCommitBody() << std::endl;

            return EXIT_SUCCESS;
        }
        else
        {
            std::cerr << "WARN: failed to get the current git state. Is this a git repo?" << std::endl;
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}
