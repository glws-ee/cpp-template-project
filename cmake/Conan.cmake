# Cpp Template Project: A template CMake project to get you started with
# C++ and tooling.
#
# Copyright 2020-2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

macro(run_conan)
  # Download automatically, you can also just copy the conan.cmake file
  if(NOT EXISTS "${CMAKE_BINARY_DIR}/conan.cmake")
    message(
      STATUS
        "Downloading conan.cmake from https://github.com/conan-io/cmake-conan"
    )
    file(
      DOWNLOAD
        "https://raw.githubusercontent.com/conan-io/cmake-conan/v0.16.1/conan.cmake"
        "${CMAKE_BINARY_DIR}/conan.cmake"
      EXPECTED_HASH
        SHA256=396e16d0f5eabdc6a14afddbcfff62a54a7ee75c6da23f32f7a31bc85db23484
      TLS_VERIFY
        ON
    )
  endif()

  set(ENV{CONAN_REVISIONS_ENABLED} 1)

  include(${CMAKE_BINARY_DIR}/conan.cmake)

  # Add (or remove) remotes as needed
  # conan_add_remote(NAME conan-center URL https://conan.bintray.com)
  conan_add_remote(NAME cci URL https://center.conan.io INDEX 0)

  conan_cmake_run(
    BASIC_SETUP
    BUILD
      missing
    CMAKE_TARGETS
    REQUIRES
      boost/1.75.0
      docopt.cpp/0.6.3
      fmt/7.1.2
      fakeit/2.0.9
      magic_enum/0.7.0
      ms-gsl/3.1.0
      quickfix/1.15.1
    GENERATORS
      cmake_find_package
    OPTIONS
      boost:without_atomic=True
      boost:without_chrono=True
      boost:without_container=True
      boost:without_context=True
      boost:without_contract=True
      boost:without_coroutine=True
      boost:without_date_time=True
      boost:without_exception=False
      boost:without_fiber=True
      boost:without_filesystem=True
      boost:without_graph_parallel=True
      boost:without_graph=True
      boost:without_iostreams=True
      boost:without_json=True
      boost:without_locale=True
      boost:without_log=True
      boost:without_math=True
      boost:without_mpi=True
      boost:without_nowide=True
      boost:without_program_options=True
      boost:without_python=True
      boost:without_random=True
      boost:without_regex=True
      boost:without_serialization=True
      boost:without_stacktrace=True
      boost:without_system=True
      boost:without_test=False
      boost:without_thread=True
      boost:without_timer=True
      boost:without_type_erasure=True
      boost:without_wave=True
      #quickfix:with_mysql=libmysqlclient
      #quickfix:with_postgres=True
      #quickfix:with_ssl=True
    SETTINGS
      compiler.cppstd=${CMAKE_CXX_STANDARD}
)
endmacro()

option(FETCH_CONAN_DEPENDENCIES "Fetch all conan packages to create a pre-build image in CI mode." OFF)
mark_as_advanced(FETCH_CONAN_DEPENDENCIES)

if(FETCH_CONAN_DEPENDENCIES)
  cmake_minimum_required(VERSION 3.15 FATAL_ERROR)

  project(ConanPackagesDownloader)

  set(CMAKE_CXX_STANDARD 17)
  set(CMAKE_CXX_STANDARD_REQUIRED ON)

  run_conan()
endif()
