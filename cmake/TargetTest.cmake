# Cpp Template Project: A template CMake project to get you started with
# C++ and tooling.
#
# Copyright 2020-2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

option(ENABLE_TESTING "Enable Test Builds" ON)
if(ENABLE_TESTING)
  enable_testing()
endif()

# This is a "meta" target that is used to collect all tests
add_custom_target(all_tests)

function(target_test name target)

  # Add the test to the CTest registry
  add_test(NAME ${name} COMMAND ${target})

  # Make the overall test meta-target depend on this test
  add_dependencies(all_tests ${target})

endfunction()
